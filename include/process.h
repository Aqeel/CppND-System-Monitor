#ifndef PROCESS_H
#define PROCESS_H

#include <string>
/*
Basic class for Process representation
It contains relevant attributes as shown below
*/
class Process {
 public:
  // Process constructor with Pid as input
  Process(int Pid);
  int Pid();                               // TODO: See src/process.cpp
  std::string User();                      // TODO: See src/process.cpp
  std::string Command();                   // TODO: See src/process.cpp
  float CpuUtilization();                  // TODO: See src/process.cpp
  std::string Ram();                       // TODO: See src/process.cpp
  long int UpTime();                       // TODO: See src/process.cpp
  bool operator<(Process const& a) const;  // TODO: See src/process.cpp
  std::string getRam() const; // const func for use in operator< 
  float getCpuUtil() const; // const func for unse in operator<

  // TODO: Declare any necessary private members
 private:
    int Pid_{00};
    float CpuUtil_{0.0};
    std::string Ram_{"0"};
    std::string Command_{"-"};
    std::string Owner_{"-"};
    int Uid_{00};
    float totalUsageTimeOld_{0.0};
    float totalTimeOld_{0.0};
};

#endif