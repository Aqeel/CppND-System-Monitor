#ifndef PROCESSOR_H
#define PROCESSOR_H

class Processor {
 public:
  float Utilization();  // TODO: See src/processor.cpp

  // TODO: Declare any necessary private members
 private:
    // store one ref of times
    int user_, nice_, system_, idle_, iowait_, irq_, softirq_, steal_, guest_, guest_nice_ = 0;
};

#endif