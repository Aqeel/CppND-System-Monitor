#include <unistd.h>
#include <cstddef>
#include <set>
#include <string>
#include <vector>

#include "process.h"
#include "processor.h"
#include "system.h"
#include "linux_parser.h"

using std::set;
using std::size_t;
using std::string;
using std::vector;

// TODO: Return the system's CPU
Processor& System::Cpu() 
{ 
    return cpu_;
    //return System::Cpu(); 
}

// TODO: Return a container composed of the system's processes
vector<Process>& System::Processes() 
{ 
    vector<int> pidTags = LinuxParser::Pids();

    Process* processI = nullptr; //objec is not yet created so ptr

    // option to save pids and update new only
    if (pidsUnique_.empty())
    {
        // populate for the first time
        for (int i : pidTags)
        {
            pidsUnique_.emplace_back(i);
            processI = new Process(i); //make a new allocation with const with Pid input
            System::processes_.emplace_back(*processI); // dereference the pointer

        }
    };

    
    for (int i : pidTags)
    {
        bool unique = true;
        for (int j : pidsUnique_)
        {
            if (i == j)
            {
                unique = false;
                break;
            }
        }
        if (unique)
        {
            processI = new Process(i); //make a new allocation with Pid input
            System::processes_.emplace_back(*processI); // dereference the pointer
            pidsUnique_.emplace_back(i);
        }
        // reset unique
        unique = true;
    }


    // code with no storage of pids
    /*processes_.clear();
    for (int i : pidTags)
    {
        processI = new Process(i); //make a new allocation with const with Pid input
        System::processes_.emplace_back(*processI); // dereference the pointer
        pidsUnique_.emplace_back(i);
    }*/


    for (auto proc : processes_)
    {
        proc.CpuUtilization(); // update cpu usage
        proc.Ram();
    }

    //std::sort(processes_.begin(), processes_.end(), std::greater<Process::Process>());
    std::sort(processes_.begin(), processes_.end(), System::sortByCpu);

    //std::sort(processes_.begin(), processes_.end());
    //std::sort(pidsUnique_.begin(), pidsUnique_.end());

    return processes_; 
}

bool System::sortByCpu(const Process &process1, const Process &process2)
{
    if (process1.getCpuUtil() < 0.001)
        return false;
    return (process1.getCpuUtil() > process2.getCpuUtil() && process1.getRam() > process2.getRam());
    //return true;
};

// TODO: Return the system's kernel identifier (string)
std::string System::Kernel() 
{ 
    return LinuxParser::Kernel(); 
}

// TODO: Return the system's memory utilization
float System::MemoryUtilization() 
{
    return LinuxParser::MemoryUtilization(); // in percentage
}

// TODO: Return the operating system name
std::string System::OperatingSystem() 
{ 
    return LinuxParser::OperatingSystem(); 
}

// TODO: Return the number of processes actively running on the system
int System::RunningProcesses() 
{ 
    return LinuxParser::RunningProcesses(); 
}

// TODO: Return the total number of processes on the system
int System::TotalProcesses() 
{
    // the size of all pids should be fine for this. Check!
    return LinuxParser::TotalProcesses(); 
}

// TODO: Return the number of seconds since the system started running
long int System::UpTime() 
{ 
    return LinuxParser::UpTime(); 
}