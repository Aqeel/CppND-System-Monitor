#include <dirent.h>
#include <unistd.h>
#include <string>
#include <vector>

#include <iostream>
#include <unistd.h>
#include <fstream>

#include "linux_parser.h"

using std::stof;
using std::string;
using std::to_string;
using std::vector;

// DONE: An example of how to read data from the filesystem
string LinuxParser::OperatingSystem() {
  string line;
  string key;
  string value;
  std::ifstream filestream(kOSPath);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::replace(line.begin(), line.end(), ' ', '_');
      std::replace(line.begin(), line.end(), '=', ' ');
      std::replace(line.begin(), line.end(), '"', ' ');
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "PRETTY_NAME") {
          std::replace(value.begin(), value.end(), '_', ' ');
          return value;
        }
      }
    }
  }
  return value;
}

// DONE: An example of how to read data from the filesystem
string LinuxParser::Kernel() {
  string os, version, kernel;
  string line;
  std::ifstream stream(kProcDirectory + kVersionFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> os >> version >> kernel;
  }
  return kernel;
}

// BONUS: Update this to use std::filesystem
vector<int> LinuxParser::Pids() {
  vector<int> pids;
  DIR* directory = opendir(kProcDirectory.c_str());
  struct dirent* file;
  while ((file = readdir(directory)) != nullptr) {
    // Is this a directory?
    if (file->d_type == DT_DIR) {
      // Is every character of the name a digit?
      string filename(file->d_name);
      if (std::all_of(filename.begin(), filename.end(), isdigit)) {
        int pid = stoi(filename);
        pids.push_back(pid);
      }
    }
  }
  closedir(directory);
  return pids;
}

// TODO: Read and return the system memory utilization
float LinuxParser::MemoryUtilization() 
{ 
  float memTotal, memFree = 0.0; // in kB
  string line;
  string line2;
  string smemory;
  string otherdata; // dummy for non essential data
  std::ifstream filestream(kProcDirectory + kMeminfoFilename);
  if (filestream.is_open())
  {
    std::getline(filestream, line); // the first line is total memory
    std::istringstream lstream(line);
    lstream >> smemory >> memTotal >> otherdata;
    
    // get the next line
    std::getline(filestream, line2); // the 2nd line is free memory
    std::istringstream lstream2(line2);
    lstream2 >> smemory >> memFree >> otherdata;

    // memory utlization = used_memory/total_memory 
    return (memTotal - memFree)/memTotal; 
  }
  
  return 0.0; 
}

// TODO: Read and return the system uptime
long LinuxParser::UpTime() 
{ 
  // in the uptime file first number is total uptime while second is total idle time
  long uptime;
  long idletime;
  string line;
  std::ifstream filestream(kProcDirectory + kUptimeFilename);
  if (filestream.is_open())
  {
    std::getline(filestream, line);
    std::istringstream sline(line);
    sline >> uptime >> idletime;
    return uptime;
  }

  return 0; 
}

// TODO: Read and return the number of jiffies for the system

// not used because dir calculated in system.cpp
long LinuxParser::Jiffies() { return 0; }

// TODO: Read and return the number of active jiffies for a PID
// REMOVE: [[maybe_unused]] once you define the function
long LinuxParser::ActiveJiffies(int pid) 
{ 
  string line;
  string lstream;
  string x;
  int utime = 0;
  int stime = 0;
  int cutime = 0;
  int cstime = 0;

  string filePath = kProcDirectory+std::to_string(pid)+kStatFilename;
  std::ifstream filestream(filePath);

  if (filestream.is_open())
  {
    std::getline(filestream, line);
    std::istringstream lstream(line);

    for (int i=0; i < 23; i++)
    {
      lstream >> x;
      if (i == 12)
      {
        lstream >> utime; // CPU user time
        lstream >> stime; // CPU time kernel
        lstream >> cutime; //
        lstream >> cstime;
        break;
      }
    }
    
    return (utime + stime + cutime + cstime);
    //return utime;
  }
  return 0; 

}

// TODO: Read and return the number of active jiffies for the system
long LinuxParser::ActiveJiffies() { return 0; }

// TODO: Read and return the number of idle jiffies for the system
long LinuxParser::IdleJiffies() { return 0; }

// TODO: Read and return CPU utilization
vector<string> LinuxParser::CpuUtilization() { return {}; }

// TODO: Read and return the total number of processes
int LinuxParser::TotalProcesses() 
{ 
  int tProcesses = 0;
  string line;
  string key;

  std::ifstream filestream(kProcDirectory + kStatFilename);
  if(filestream.is_open())
  {
    while(std::getline(filestream,line))
    {
      std::istringstream lstream(line);
      while(lstream >> key >> tProcesses) // only check for first value (string) & second (int)
      {
          if (key == "processes")
            return tProcesses;
      }
    }
  }
  return 0;
}

// TODO: Read and return the number of running processes
int LinuxParser::RunningProcesses() 
{ 
  int runningProcesses = 0;
  string line;
  string key;

  std::ifstream filestream(kProcDirectory + kStatFilename);
  if (filestream.is_open())
  {
    while(std::getline(filestream, line))
    {
      std::istringstream lstream(line);
      while(lstream >> key >> runningProcesses)
      {
        if (key == "procs_running")
        return runningProcesses;
      }
    }
  }

  return 0; 
}

// TODO: Read and return the command associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Command(int pid) 
{ 
  string line;

  string filePath = kProcDirectory+std::to_string(pid)+kCmdlineFilename;
  std::ifstream filestream(filePath);

  if (filestream.is_open())
  {
    std::getline(filestream, line);
    return line;
  }
  return string(); 
}

// TODO: Read and return the memory used by a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Ram(int pid) 
{ 
  string line;
  string lstream;
  string key;
  int mvalue;

  string filePath = kProcDirectory+std::to_string(pid)+"/status";
  std::ifstream filestream(filePath);

  if (filestream.is_open())
  {
    while(std::getline(filestream, line))
    {
      std::istringstream lstream(line);
      while (lstream >> key >> mvalue)
      {
        if (key == "VmSize:")
          return std::to_string(mvalue/1000);
      }
    }
  }

  return string();
}

// TODO: Read and return the user ID associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Uid(int pid) 
{
  string line;
  string lstream;
  string key;
  int uValue;

  string filePath = kProcDirectory+std::to_string(pid)+"/status";
  std::ifstream filestream(filePath);

  if (filestream.is_open())
  {
    while(std::getline(filestream, line))
    {
      std::istringstream lstream(line);
      while (lstream >> key >> uValue)
      {
        if (key == "Uid:")
          return std::to_string(uValue);
      }
    }
  } 
  return string(); 
}

// TODO: Read and return the user associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::User(int pid) 
{
  
  string Uid = LinuxParser::Uid(pid);

  string user;
  string line;
  string lstream;
  string key, x, value;
  
  string filePath = kPasswordPath;
  std::ifstream filestream(filePath);

  if (filestream.is_open())
  {
    while(std::getline(filestream, line))
    {
      std::replace(line.begin(), line.end(), ':', ' ');
      std::istringstream lstream(line);
      while (lstream >> key >> x >> value)
      {
        if (value == Uid)
          return key;
      }
    }
  }

  return string(); 
}

// TODO: Read and return the uptime of a process
// REMOVE: [[maybe_unused]] once you define the function
long LinuxParser::UpTime(int pid) 
{

  string line;
  string lstream;
  string x;

  int tvalue;
  //int pidValue;

  string filePath = kProcDirectory+std::to_string(pid)+kStatFilename;
  std::ifstream filestream(filePath);

  if (filestream.is_open())
  {

    std::getline(filestream, line);
    //std::replace(line.begin(), line.end(), '(', 'a');
    std::istringstream lstream(line);
    for (int i=0; i < 23; i++)
    {
      lstream >> x;
      if (i == 20)
      {
        lstream >> tvalue;  // move one more time to 21st index (22nd value)
        break;
      }
    }
    
    // a test for time values
    /*std::ofstream vecFile("vect.txt");
    if(vecFile.is_open())
    {
      vecFile << "PID\n";
      vecFile << pid << "\n";
      //vecFile << tvalue;
      vecFile << "tValue\n";
      vecFile << tvalue << "\n";
      vecFile.close();
    }*/

    return tvalue;
  }



  return 0; 

}