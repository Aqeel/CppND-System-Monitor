#include <unistd.h>
#include <cctype>
#include <sstream>
#include <string>
#include <vector>

#include <iostream>

#include "process.h"
#include "linux_parser.h"


using std::string;
using std::to_string;
using std::vector;


Process::Process(int Pid): Pid_(Pid)
{
}; 

// TODO: Return this process's ID
int Process::Pid() 
{ 
    return Pid_;
}

// TODO: Return this process's CPU utilization
float Process::CpuUtilization() 
{

    float totalTimeJiffs = LinuxParser::ActiveJiffies(Pid_)+0.0;

    float totalUsageTime = totalTimeJiffs/sysconf(_SC_CLK_TCK);
    
    float totalTime = LinuxParser::UpTime() - LinuxParser::UpTime(Pid_)/sysconf(_SC_CLK_TCK) + 0.0; // system- process time
    //float totalTime = LinuxParser::UpTime(Pid_)/sysconf(_SC_CLK_TCK) + 0.0; // only the process time
    //float cpuUsage = totalUs +ageTime/totalTime;

    float cpuUsage = (totalUsageTime - totalUsageTimeOld_)/(totalTime - totalTimeOld_);
    //float cpuUsage = (totalUsageTime - totalUsageTimeOld_)/totalTime;

    CpuUtil_ = cpuUsage;

    totalUsageTimeOld_ = totalUsageTime;
    totalTimeOld_ = totalTime;


    return cpuUsage;

    //return totalTimeJiffs; 
}

// TODO: Return the command that generated this process
string Process::Command() 
{
    //if (Process::Command_ == "-")
       // Process::Command_ = LinuxParser::Command(Pid()); 
    return LinuxParser::Command(Pid_); 
}

// TODO: Return this process's memory utilization
string Process::Ram() 
{
    Process::Ram_ = LinuxParser::Ram(Pid_);
    //return Process::Ram_;
    return LinuxParser::Ram(Pid());
}

string Process::getRam() const
{
    return Process::Ram_;
}

float Process::getCpuUtil() const
{
    return Process::CpuUtil_;
}

// TODO: Return the user (name) that generated this process
string Process::User() 
{
    if (Process::Owner_ == "-")
        Process::Owner_ = LinuxParser::User(Pid());
    return LinuxParser::User(Pid());
    //return Process::Owner_ ;
}

// TODO: Return the age of this process (in seconds)
long int Process::UpTime() 
{
    long int ttime =  LinuxParser::UpTime(Pid());
    long int timeU;
    //return LinuxParser::UpTime(Pid());
        //divide the "clock ticks" value by sysconf(_SC_CLK_TCK)
    timeU = ttime/sysconf(_SC_CLK_TCK);

    //long systemUpTime = LinuxParser::UpTime();
    
    //return (systemUpTime-timeU); 
    return timeU;
}

// TODO: Overload the "less than" comparison operator for Process objects
// REMOVE: [[maybe_unused]] once you define the function
//bool Process::operator<(Process const& a[[maybe_unused]]) const { return true; }
bool Process::operator<(Process const& a) const 
{
    bool cond = false;

    float n1 = 0.0;
    float n2 = 0.0;
    int r1 = 0;
    int r2 = 0;

    n1 = getCpuUtil();
    n2 = a.getCpuUtil();

    std::string s1 = getRam(); // access using const function
    std::string s2 = a.getRam();
    r1 = std::stoi(s1);
    r2 = std::stoi(s2);

    if (n2 > n1 && r1 > r2)
        cond = true;
    
    return cond;  
}