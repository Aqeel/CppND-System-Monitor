#include "processor.h"
#include "linux_parser.h"
#include "unistd.h"

// TODO: Return the aggregate CPU utilization
float Processor::Utilization() 
{ 
    std::string line;
    std::string cpuAgg;
    int user, nice, system, idle, iowait, irq, softirq, steal, guest, guest_nice = 0;
    float cpuUtil;

    int wait = 10; // time in microseconds for update of CPU load

    int totalIdleTime = 0;
    int totalTime = 0;
    int totalIdleTimeOld = 0;
    int totalTimeOld = 0;

    /*
    user: normal processes executing in user mode
    nice: niced processes executing in user mode
    system: processes executing in kernel mode
    idle: twiddling thumbs
    iowait: In a word, iowait stands for waiting for I/O to complete
    irq: servicing interrupts
    softirq: servicing softirqs
    steal: involuntary wait
    guest: running a normal guest
    guest_nice: running a niced guest
    */

    std::string filePath = (LinuxParser::kProcDirectory + LinuxParser::kStatFilename);
    //std::ifstream filestream(filePath);

    if (Processor::user_ == 0)
    {
        std::ifstream filestream(filePath);
        while(filestream.is_open())
        {
            std::getline(filestream, line);
            std::istringstream lstream(line); // get only the first line

            lstream >> cpuAgg >> user >> nice >> system >> idle >> iowait >> irq >> softirq >> steal >> guest >> guest_nice;

            // set private members
            user_ = user; nice_ = nice; system_ = system; idle_ = idle;
            iowait_ = iowait; irq_ = irq; softirq_ = softirq; steal_ = steal; guest_ = guest; guest_nice_ = guest_nice;

            totalIdleTime = idle + iowait;
            totalTime = user + nice + system + irq + softirq + steal + guest + guest_nice + idle + iowait;

            cpuUtil = (totalTime - totalIdleTime)/totalTime;

            return cpuUtil;

        }
    }
    else
    {
        usleep(wait); // works for Unix systems only
        std::ifstream filestream(filePath);
        while(filestream.is_open())
        {
            std::getline(filestream, line);
            std::istringstream lstream(line); // get only first line

            lstream >> cpuAgg >> user >> nice >> system >> idle >> iowait >> irq >> softirq >> steal >> guest >> guest_nice;

            totalIdleTime = idle + iowait;
            totalTime = user + nice + system + irq + softirq + steal + guest + guest_nice + idle + iowait;

            totalIdleTimeOld = idle_ + iowait_;
            totalTimeOld = user_ + nice_ + system_ + irq_ + softirq_ + steal_ + guest_ + guest_nice_ + idle_ + iowait_;

            // update private members
            user_ = user; nice_ = nice; system_ = system; idle_ = idle;
            iowait_ = iowait; irq_ = irq; softirq_ = softirq; steal_ = steal; guest_ = guest; guest_nice_ = guest_nice;

            float tTimeDiff = totalTime-totalTimeOld;
            float tIdleDiff = totalIdleTime-totalIdleTimeOld;

            if (tTimeDiff > 0 && tIdleDiff > 0 && (tTimeDiff - tIdleDiff) > 0) // check to avoid invalid update
            {
                cpuUtil = (tTimeDiff - tIdleDiff)/tTimeDiff;
            }
            else
            {
                cpuUtil = (totalTime - totalIdleTime)/totalTime;
            }

            return cpuUtil;

        }
    }
    
    return 0.0; 
}