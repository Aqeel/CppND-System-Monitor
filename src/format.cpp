#include <string>

#include "format.h"

using std::string;

// TODO: Complete this helper function
// INPUT: Long int measuring seconds
// OUTPUT: HH:MM:SS
// REMOVE: [[maybe_unused]] once you define the function
string Format::ElapsedTime(long seconds) 
{ 
    int hours, mins, secs = 0;
    
    hours = seconds/3600;
    mins = (seconds - hours*3600)/60;
    secs = seconds - mins*60 - hours*3600;

    std::string shrs = std::to_string(hours);
    std::string smins = std::to_string(mins);
    std::string ssecs = std::to_string(secs);

    //pretty print for second digit
    if (hours < 10)
        shrs = "0"+shrs;
    if (mins < 10)
        smins = "0"+smins;
    if (secs < 10)
        ssecs = "0"+ssecs;
  
    string stime;
    //stime = shrs + ":" + smins + ":"  + std::to_string(secs) + " total secs: " + std::to_string(seconds);
    stime = shrs + ":" + smins + ":" + ssecs;
    return stime; 
}